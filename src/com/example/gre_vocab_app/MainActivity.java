package com.example.gre_vocab_app;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button start_test = (Button) findViewById(R.id.start_test);
		start_test.setTextSize(20);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
        
    }
    
    public void clickStart(View view)
    {
    	Intent intent = new Intent(this, TestVocab.class);
		startActivity(intent);
    }
    
    public void clickLearn(View view)
    {
    	Intent intent = new Intent(this, LearnVocab.class);
		startActivity(intent);
    }
}
