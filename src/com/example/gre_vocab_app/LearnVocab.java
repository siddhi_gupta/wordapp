package com.example.gre_vocab_app;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;

import com.example.gre_vocab_app.VocabContent.node;

import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

   public class LearnVocab extends Activity {
	
	TextView word, meaning;
	ArrayList<node> dict;	
	String[] entry;
	VocabContent vc;
	

	@SuppressLint("NewApi") @Override
	protected void onCreate(Bundle savedInstanceState) {
		
		vc = new VocabContent();
		Context context = getApplicationContext();	
		try {
			InputStream inp_words = context.getAssets().open("barron_words.txt");
			dict = vc.makeList(inp_words);
			entry = vc.getTestEntry(dict);
			} catch (IOException e) {
			e.printStackTrace();
			
			
		}
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_learn_vocab);
		
		Button next1 = (Button) findViewById(R.id.button2);
		Button exit1 = (Button) findViewById(R.id.button3);
		word= (TextView) findViewById(R.id.definition);
		meaning= (TextView) findViewById(R.id.textView1);
		word.setText(entry[7]);
		meaning.setText(entry[0]);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.learn_vocab, menu);
		return true;
	}

	public void next (View view)
	{
		entry = vc.getTestEntry(dict);
		word.setText(entry[7]);
		meaning.setText(entry[0]);	
		
		
	}
	public void exit(View view)
	{
		Intent intent = new Intent(this, MainActivity.class);
		startActivity(intent);
	}
	
}
