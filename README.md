General Instructions
================
--------------------------------

1. Clone this repo directly. 
2. Edit the script using Sublime or any other text editor.
3. Have the input file in the required format.
4. Run the project with Android SDK.
Use of Readme
============
------------------------------

* Use Table of Contents to navigate through the sections
* Review the edit log for recent changes
	* Remember to add you signature and date after you review recent changes!
* When you make a change, create an entry at the top of the Edit Log with the date of the change and your signature.

Links
====
----------

* [Android SDK](http://developer.android.com/sdk/installing/index.html)
* [Barrons word List](https://quizlet.com/47571/barrons-gre-wordlist-4759-words-flash-cards/)

Table of Contents
==============
----------------------------------
[TOC]

WordApp
===================
----------------------------------------

## Problem Description

This is an Android application to help users improve their vocabulary with 2 modes - learning and testing. 
The learning mode includes flashcards to familiarize with new words and testing has quizzes to ensure the recall of the user of those words.

Edit Log
=======
-----------------
## Complete Creation
> Date: 04/03/14
> 
> **Sections:**
> 
> * General Instructions
> * Use of Readme
> * Links
> * Problem Statement
>
>
> **Read Signature**
> 
> * Siddhi Gupta: 04/03/14